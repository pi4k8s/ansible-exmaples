# 自动化部署示例

#### 介绍
自动化部署脚本汇总
- [ansible 安装 docker](https://my.oschina.net/fastjrun/blog/5523451) 
    - 修改日期：2025-02-01 
- [ansible 安装 python3 ](https://my.oschina.net/fastjrun/blog/17432090)
    - 创建日期：2025-02-01 
- [基于 ansible 在远程 centos 服务器 docker 环境安装 docker-redis](https://my.oschina.net/fastjrun/blog/5523452)
- [基于 ansible 在远程 centos 服务器 docker 环境安装 docker-mysql](https://my.oschina.net/fastjrun/blog/5523453)
- [基于 ansible 在远程 centos 服务器 docker 环境安装 docker-nginx](https://my.oschina.net/fastjrun/blog/5525587)
- [基于 ansible 在远程 centos 服务器 docker 环境安装kafka ](https://ost.51cto.com/posts/13702)


