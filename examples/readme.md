### docker

```bash
ansible-playbook playbook/install-host-role.yaml -i hosts  -e role_name=docker -e env_hosts=aliyun155
```

### python3
```bash
ansible-playbook playbook/install-host-role.yaml -i hosts -e role_name=python3 -e env_hosts=aliyun155 
```

### docker-mysql-install
```bash
ansible-playbook playbook/install-host-role.yaml -i hosts -e role_name=docker-mysql-install \
    -e env_hosts=aliyun155 -e env_var_file=docker-mysql-install/vars/mysql8.yml 
```
### docker-mysqldb-init
 
```bash
ansible-playbook playbook/install-host-role.yaml -i hosts -e env_hosts=aliyun155 -e role_name=docker-mysqldb-init \
    -e env_var_file=docker-mysqldb-init/vars/aliyun155-typecho.yml
```

```bash
ansible-playbook playbook/install-host-role.yaml -i hosts -e env_hosts=aliyun155 -e role_name=docker-mysqldb-init \
    -e env_var_file=docker-mysqldb-init/vars/aliyun155-moodle45.yml
```

### docker-moodle-install
```bash
ansible-playbook playbook/install-host-role.yaml -i hosts -e env_hosts=aliyun155 -e role_name=docker-moodle-install \
    -e env_var_file=docker-moodle-install/vars/aliyun155.yml
```

### complex

#### python3-docker
```bash
ansible-playbook playbook/complex/python3-docker/python3-docker.yaml -i hosts -e env_hosts=aliyun155
```